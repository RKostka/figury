package anim;

import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;

public class Rliterka extends Figura {

	public Rliterka(Graphics2D buf, int del, int w, int h) {
		super(buf, del, w, h);
		int[] Xcoor ={0, 5,  5,10,15, 10, 15, 15, 10,  5,  5,  8, 11, 11,  8,  5,  5, 0};
		int[] Ycoor ={0, 0,-14, 0, 0,-14,-18,-26,-30,-30,-18,-18,-20,-24,-26,-26,-30,-30};
		shape = new Polygon(Xcoor,Ycoor,18);
		area = new Area(shape);
		aft = new AffineTransform();
		// TODO Auto-generated constructor stub
	}

}
