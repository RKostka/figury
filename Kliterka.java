package anim;

import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;

public class Kliterka extends Figura {

	public Kliterka(Graphics2D buf, int del, int w, int h) {
		super(buf, del, w, h);
		int[] Xcoor ={0,5,5 ,10,15,9,15,10,5,5,0};
		int[] Ycoor ={0,0,12,0 , 0,15,30,30,18,30,30};
		shape = new Polygon(Xcoor,Ycoor,11);
		area = new Area(shape);
		aft = new AffineTransform();
	}

}
